package it.matteoavanzini.myapplicationtoolbar.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import it.matteoavanzini.myapplicationtoolbar.R;
import it.matteoavanzini.myapplicationtoolbar.adapter.ItemRecyclerViewAdapter;
import it.matteoavanzini.myapplicationtoolbar.fragment.dummy.DummyContent;

/**
 * A fragment representing a list of Items.
 */
public class ItemFragment extends Fragment {

    public static final int VIEW_GRID = 1;
    public static final int VIEW_LIST = 0;

    private static final String ARG_VIEW_MODE = "view_mode";
    private ItemRecyclerViewAdapter mAdapter;
    private int mMode;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ItemFragment() {

    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static ItemFragment newInstance(int mode) {
        ItemFragment fragment = new ItemFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_VIEW_MODE, mode);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mMode = getArguments().getInt(ARG_VIEW_MODE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_item_list_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            if (mMode == VIEW_LIST) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, 2));
            }
            mAdapter = new ItemRecyclerViewAdapter(DummyContent.ITEMS);
            recyclerView.setAdapter(mAdapter);
        }
        return view;
    }

    public void refresh() {
        if (mAdapter != null)
            mAdapter.notifyDataSetChanged();
    }
}