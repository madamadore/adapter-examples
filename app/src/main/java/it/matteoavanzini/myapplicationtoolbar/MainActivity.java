package it.matteoavanzini.myapplicationtoolbar;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;
import it.matteoavanzini.myapplicationtoolbar.adapter.TabAdapter;
import it.matteoavanzini.myapplicationtoolbar.fragment.ItemFragment;
import it.matteoavanzini.myapplicationtoolbar.fragment.dummy.DummyContent;

public class MainActivity extends AppCompatActivity {

    private ItemFragment listFragment;
    private ItemFragment gridFragment;
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Mio Titolo");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(android.R.drawable.ic_menu_more);

        NavigationView mNavigation = findViewById(R.id.navigation_view);
        mNavigation.setNavigationItemSelectedListener(new MyNavigationListener());



        mViewPager = (ViewPager) findViewById(R.id.viewPager);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabLayout);

        listFragment = ItemFragment.newInstance(ItemFragment.VIEW_LIST);
        gridFragment = ItemFragment.newInstance(ItemFragment.VIEW_GRID);

        TabAdapter adapter = new TabAdapter(this, getSupportFragmentManager());
        adapter.addFragment(listFragment, "Lista", android.R.drawable.arrow_down_float);
        adapter.addFragment(gridFragment, "Griglia", android.R.drawable.arrow_up_float);

        mViewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(mViewPager);

    }

    public void removeElement(View view) {
        DummyContent.ITEMS.remove(5);
        listFragment.refresh();
        gridFragment.refresh();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        return handleMenuClick(item);
    }

    class MyNavigationListener implements NavigationView.OnNavigationItemSelectedListener {


        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            return handleMenuClick(item);
        }

    }

    // metodo per gestire i click sugli "item" del menu
    private boolean handleMenuClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_locate:
                // .....
                break;
            case R.id.action_preferred:
                // ----
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}