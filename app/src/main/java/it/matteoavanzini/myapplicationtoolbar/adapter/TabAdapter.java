package it.matteoavanzini.myapplicationtoolbar.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.DynamicDrawableSpan;
import android.text.style.ImageSpan;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.lifecycle.Lifecycle;

public class TabAdapter extends FragmentStatePagerAdapter {
    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();
    private final List<Drawable> mFragmentIconList = new ArrayList<>();
    private final Context mContext;

    public TabAdapter(Context context, FragmentManager fm) {
        super(fm, Lifecycle.State.RESUMED.ordinal());
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    public void addFragment(Fragment fragment, String title, int iconResource) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
        mFragmentIconList.add(mContext.getResources().getDrawable(iconResource));
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        String title = mFragmentTitleList.get(position);
        SpannableStringBuilder sb = new SpannableStringBuilder("   " + title);
        Drawable myDrawable = mFragmentIconList.get(position);

        myDrawable.setBounds(5, 5, myDrawable.getIntrinsicWidth(), myDrawable.getIntrinsicHeight());
        ImageSpan span = new ImageSpan(myDrawable, DynamicDrawableSpan.ALIGN_BASELINE);
        sb.setSpan(span, 0, 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        return sb;
    }


    @Override
    public int getCount() {
        return mFragmentList.size();
    }
}
